""" This file has the class declaration and the functions for handling the go-kart state update for each time step of
the simulation"""

from GoKartSimParam import GoKartParam as gp
from GoKartSimParam import SimParam as sim
import numpy as np
import math


class GoKartSim_Step(object):

    def __init__(self, samp_time):

        self.samp_time = sim.samp_time  # secs

    def centri_update(self, pos_array):  # Pos Array has last 3 positions

        self.rad = None
        self.pos_centre_vec = np.array([[None], [None]])
        m_1 = 0.0
        m_2 = 0.0
        pos_array = np.transpose(pos_array)
        if not np.array_equal(pos_array[:, 0], pos_array[:, 1]) and not np.array_equal(pos_array[:, 1], pos_array[:, 2]):
            m_1 = (pos_array[1, 1] - pos_array[1, 0]) / (pos_array[0, 1] - pos_array[0, 0])
            m_2 = (pos_array[1, 2] - pos_array[1, 1]) / (pos_array[0, 2] - pos_array[0, 1])
        if m_1 != m_2 and m_1 != 0:

            x_pos = (m_1*m_2*(pos_array[1, 0] - pos_array[1, 2]) + m_2*(pos_array[0, 0] + pos_array[0, 1])
                     - m_1*(pos_array[0, 1] + pos_array[0, 2])) / (2*(m_2 - m_1))

            y_pos = -(x_pos - (pos_array[0, 0] + pos_array[0, 1]) / 2) / m_1 + (pos_array[1, 0] + pos_array[1, 1]) / 2

            self.rad = math.sqrt((x_pos - pos_array[0, 0]) ** 2 + (y_pos - pos_array[1, 0]) ** 2)

            self.pos_centre_vec = np.array([[x_pos], [y_pos]])

        return self.pos_centre_vec, self.rad

    def accel_update(self, pos_centre_vec, rad, pos_array, vel, tang_accel, throttle, steer):  # Pos Array has last 2 positions

        centri_accel = 0.0
        theta_tang = 0.0
        self.theta_kart = 0.0
        tang_accel = tang_accel + throttle
        pos_array = np.transpose(pos_array)
        if rad is not None and pos_centre_vec[0, 0] != pos_array[0, 1]:
            centri_accel = vel**2 / rad
            theta_tang = math.atan((pos_centre_vec[1, 0] - pos_array[1, 1]) /
                                   (pos_centre_vec[0, 0] - pos_array[0, 1]))
            self.theta_kart = math.atan((pos_array[1, 1] - pos_array[1, 0]) / (pos_array[0, 1] - pos_array[0, 0]))
        self.theta_kart += steer
        accel_x = (tang_accel * math.cos(self.theta_kart)) + (centri_accel * math.cos(theta_tang))
        accel_y = (tang_accel * math.sin(self.theta_kart)) + (centri_accel * math.sin(theta_tang))
        self.accel_vec_update = np.array([[accel_x], [accel_y]])
        self.accel_vec_update, self.accel_mag_update, accel_max_bool = \
                                                            self.kart_accel_limit_validation(self.accel_vec_update)

        return self.accel_vec_update, self.accel_mag_update, self.theta_kart, accel_max_bool

    def pos_update(self, pos_vec, vel_vec, accel_vec):

        pos_mat = np.array([pos_vec])
        vel_mat = np.transpose(np.array(vel_vec))
        accel_mat = np.transpose(accel_vec)
        disp_vec = (vel_mat * self.samp_time) + ((accel_mat * self.samp_time**2) / 2)
        self.pos_upd = pos_mat + disp_vec

        return self.pos_upd

    def vel_update(self, vel_vec, accel_vec):

        self.vel_vec_update = vel_vec + accel_vec * self.samp_time
        self.vel_vec_update, self.vel_mag_update = self.kart_vel_limit_validation(self.vel_vec_update)

        return self.vel_vec_update, self.vel_mag_update

    def kart_accel_limit_validation(self, accel):

        accel_mag = math.sqrt(accel[0, 0] ** 2 + accel[1, 0] ** 2)
        factor = accel_mag/gp.max_accel
        max_bool = False

        if factor > 1.0:
            accel = accel/factor
            accel_mag = accel_mag/factor
            max_bool = True

        return accel, accel_mag, max_bool

    def kart_vel_limit_validation(self, vel):

        vel_mag = math.sqrt(vel[0, 0] ** 2 + vel[1, 0] ** 2)
        factor = vel_mag/gp.max_vel

        if factor > 1.0:
            vel = vel/factor
            vel_mag = vel_mag/factor

        return vel, vel_mag




