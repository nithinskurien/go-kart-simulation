""" This class is responsible for keeping track information and the Go-Kart's position with respect to the track.
The class also has checkpoint information."""

import numpy as np
import pandas as pd
import math
from GoKartSimParam import SimParam as sim


class GoKartTrack(object):

    def __init__(self):

        self.track = []
        self.spline = []
        self.spline_split_array = []
        self.track_bound_a = []
        self.track_bound_b = []
        self.track_angle = []
        self.file_name = None
        self.spline_table = pd.DataFrame()

    def create_track_spline(self):

        self.spline = [[0, 0], [200, 0]]
        self.create_quadrant(201, 300, 1)
        self.create_quadrant(300, 199, -1)
        self.spline_table = pd.DataFrame(self.spline, columns=sim.track_columns)

    def split_spline(self, split, spline=None):

        self.spline_split_array = []
        if spline is None:
            spline = self.spline
        self.spline_split_array.append(spline[0])
        pt_start = spline[0]
        index = 1
        while pt_start != spline[-1]:
            dist = split
            while dist > 0.0:
                dist_moved, pt_end = self.move_distance_between_pts(pt_start, self.spline[index], dist)
                dist -= dist_moved
                pt_start = pt_end
                if pt_end == self.spline[index]:
                    index += 1
                if index == len(spline):
                    break
            self.spline_split_array.append(pt_end)

        return self.spline_split_array

    def track_boundary(self, spline=None, angle=False):

        self.track_bound_a = []
        self.track_bound_b = []
        if angle:
            self.track_angle = []
        if spline is None:
            spline = self.spline_split_array
        index = -1
        direction = 1
        while True:
            index += 1
            slope = 0.0
            if spline[index] != spline[-1] and index != 0:
                slope = self.quadratic_tangent(spline[index-1:index+2])
            pt_a, pt_b, direction = self.equidistant_pts_tangent(spline[index], sim.track_width/2, slope, direction)
            self.track_bound_a.append(pt_a)
            self.track_bound_b.append(pt_b)
            if angle:
                self.track_angle.append(math.atan(slope))
            if spline[index] == spline[-1]:
                break
        self.track_bound_a, self.track_bound_b = self.find_inner_track(self.track_bound_a, self.track_bound_b)
        if angle:
            return self.track_bound_a, self.track_bound_b, self.track_angle
        if not angle:
            return self.track_bound_a, self.track_bound_b


    def equidistant_pts_tangent(self, pt, dist, slope, direction):

        if slope != 0.0:
            slope = -1/slope
            mat_pt = np.array([[pt[0]], [pt[1]]])
            mat_a = np.array([[1, 0], [0, 1], [1, 0], [0, 1]])
            mat_b = np.array([[1], [slope], [-1], [-slope]])
            b_const = dist / math.sqrt(1 + slope ** 2)
            equi_pts = mat_a.dot(mat_pt) + (b_const * mat_b)
            direction = -slope / abs(slope)
        else:
            equi_pts = [[pt[0]], [pt[1] - direction * dist], [pt[0]], [pt[1] + direction * dist]]

        return [equi_pts[0][0], equi_pts[1][0]], [equi_pts[2][0], equi_pts[3][0]], direction


    def dist_between_pts(self, pt_a, pt_b):

        distance = 0.0
        pt_a = np.array(pt_a)
        pt_b = np.array(pt_b)
        if not np.array_equal(pt_a, pt_b):
            distance = math.sqrt((pt_b[0] - pt_a[0])**2 + (pt_b[1] - pt_a[1])**2)

        return distance

    def move_distance_between_pts(self, pt_a, pt_b, dist):

        point = pt_b
        dist_pts = self.dist_between_pts(pt_a, pt_b)
        pt_a = np.array(pt_a)
        pt_b = np.array(pt_b)
        line_vector = pt_b - pt_a
        if dist < dist_pts:
            norm_vector = line_vector / dist_pts
            point = pt_a + (dist * norm_vector)
            dist_pts = dist

        return dist_pts, [point[0], point[1]]

    def save_track(self, file_name):

        self.file_name = file_name
        if self.file_name is not None:
            self.spline_table.to_csv(self.file_name, sep=',', encoding='utf-8', index=False)
            print('Track exported')

        else:
            print('Please enter a valid file name and directory')

    def load_track(self, file_name):

        self.spline_table = pd.read_csv(file_name, sep=',')
        if self.spline_table.empty:
            print('Track information is empty ')

        elif list(self.spline_table.columns.values) != sim.track_columns:
            print('Wrong track file selected')

        else:
            self.spline_table[sim.track_columns] = self.spline_table[sim.track_columns].astype(float)
            x_array = self.spline_table['x'].tolist()
            y_array = self.spline_table['y'].tolist()
            temp = np.transpose([x_array, y_array])
            self.spline = temp.tolist()
            print('The track was loaded successfully')

        return self.spline

    def create_quadrant(self, x_start, x_end, increment):

        y_value = 0
        for x_value in range(x_start, x_end, increment):
            y1 = 100 - math.sqrt(-x_value ** 2 + 400 * x_value - 30000)
            y2 = math.sqrt(-x_value ** 2 + 400 * x_value - 30000) + 100
            slope = 0
            if self.spline[-1][1] - self.spline[-2][0] != 0:
                slope = (self.spline[-1][1] - self.spline[-2][1]) / (self.spline[-1][1] - self.spline[-2][0])
            slope_y1 = (y1 - self.spline[-1][1]) / (x_value - self.spline[-1][0])
            slope_y2 = (y2 - self.spline[-1][1]) / (x_value - self.spline[-1][0])
            y_value = y1
            if abs(slope_y1 - slope) > abs(slope_y2 - slope):
                y_value = y2
            self.spline.append([x_value, y_value])


    def quadratic_tangent(self, point_array):  # Computes the tangent to the curve formed by the points in the point_array at point_array[1].

        matrix = np.array([[point_array[0][0]**2, point_array[0][0], 1],
                           [point_array[1][0]**2, point_array[1][0], 1],
                           [point_array[2][0]**2, point_array[2][0], 1]])
        matrix = np.linalg.inv(matrix)
        y_vector = np.array([[point_array[0][1]], [point_array[1][1]], [point_array[2][1]]])
        coeff = np.matmul(matrix, y_vector)
        diff = 2*coeff[0][0] * point_array[1][0] + coeff[1][0]
        return diff

    def goKart_between_checkpoints(self, pt, checkpoint_box):

        n = len(checkpoint_box)
        inside = False
        p1_x, p1_y = checkpoint_box[0]
        for i in range(1, n+1):
            p2_x, p2_y = checkpoint_box[i % n]
            if pt[0, 1] > min(p1_y, p2_y):
                if pt[0, 1] <= max(p1_y, p2_y):
                    if pt[0, 0] <= max(p1_x, p2_x):
                        if p1_y != p2_y:
                            x_on_line = (pt[0, 1] - p1_y)*(p2_x - p1_x)/(p2_y - p1_y) + p1_x
                        if p1_x == p2_x or pt[0, 0] <= x_on_line:
                            inside = not inside
            p1_x, p1_y = p2_x, p2_y

        return inside

    def proj_point_line_segment(self, pt1, pt2, pt3):  # Project point P3 onto line connecting P1 and P2 return that point

        slope = 0.0
        lamb = pt3[0, 0]

        if pt2[0] != pt1[0]:
            slope = (pt2[1] - pt1[1]) / (pt2[0] - pt1[0])

        if slope != 0:
            lamb = (slope * (pt3[0, 1] - pt1[1]) + (pt3[0, 0] - pt1[0])) / (1 + slope ** 2)

        proj = [pt1[0] + lamb, pt1[1] + (lamb*slope)]

        if pt2[0] == pt1[0]:
            proj = [pt2[0], pt3[0, 1]]

        return proj

    def find_inner_track(self, track_a, track_b):  # Find the inner track by finding the total distance of the both track

        dist_a = self.return_track_distance(track_a)
        dist_b = self.return_track_distance(track_b)
        if dist_b < dist_a:
            temp = track_a
            track_a = track_b
            track_b = temp

        return track_a, track_b

    def return_track_distance(self, list_array):  # Find the total distance of the array by combining the distance between each element

        total_dist = 0
        for itera in range(len(list_array)-1):
            total_dist += self.dist_between_pts(list_array[itera], list_array[itera+1])

        return total_dist

    def check_on_track(self, pos, inner_track, outer_track, track_index):

        inside = False
        distance = 0.0
        track_pos_temp = 0.0
        for index in range(track_index, len(inner_track)-1):
            box = [inner_track[index], inner_track[index+1], outer_track[index+1], outer_track[index]]
            if self.goKart_between_checkpoints(pos, box):
                distance, track_pos_temp = self.goKart_projection_distance(pos, inner_track[index:index+2],
                                                                           outer_track[index:index+2])
                inside = True
                break
        closest_track_index = int(index+track_pos_temp)

        if not inside:

            closest_track_index = track_index

        return inside, int(index), distance, closest_track_index  # returns the boolean, checkpoint index, distance of goKart from the inner track and the index of the track which is closest to the position of the kart

    def goKart_projection_distance(self, pos, inner_track, outer_track):

        track_pos  = 0
        distance = sim.track_checkpoint_distance

        for index in range(len(inner_track)):

            projection = self.proj_point_line_segment(inner_track[index], outer_track[index], pos)
            distance_temp = self.dist_between_pts(projection, inner_track[index])

            if distance_temp < distance:
                distance = distance_temp
                track_pos = index

        return distance, track_pos

    def diffrential_angle_curve_horizon(self, angle_track, index):

        diff = 0.0

        for iter in range(index + sim.track_horizon_sight, index + sim.track_diff_horizon + sim.track_horizon_sight):
            diff += angle_track[iter + 1] - angle_track[index]

        return diff
