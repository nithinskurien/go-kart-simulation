""" This class is responsible for plotting and displaying important information and helping in the visualization
of the track and Go-Kart states."""

import numpy as np
import matplotlib.pyplot as plt


def plot_track(track_1=None, track_2=None, track_3=None):
    if track_1 is not None:
        track_1 = np.array(track_1)
        track_1 = np.transpose(track_1)
        plt.plot(track_1[0], track_1[1], 'b')
    if track_2 is not None:
        track_2 = np.array(track_2)
        track_2 = np.transpose(track_2)
        plt.plot(track_2[0], track_2[1], 'r')
    if track_3 is not None:
        track_3 = np.array(track_3)
        track_3 = np.transpose(track_3)
        plt.plot(track_3[0], track_3[1], 'r')


def plot_checkpoints(track_a, track_b):
    for index in range(len(track_a)):
        plt.plot([track_a[index][0], track_b[index][0]], [track_a[index][1], track_b[index][1]], 'b', alpha=0.5)

def plot_gokart_pos(pos_array):
    pos_mat = np.array(pos_array).transpose()
    plt.plot(pos_mat[0, 2:], pos_mat[1, 2:], 'g')

def show_plots():
    plt.grid()
    plt.show()
