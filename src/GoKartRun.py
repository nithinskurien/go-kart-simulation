import GoKartSim_Step as gs
from GoKartSim import GoKartSim as sim
from GoKartSimParam import SimParam as sim_param
import GoKartPlot as disp
import GoKartQTable as q
import GoKartTrack as track
import numpy as np
import time

qTable_file_name = '../analysis/GoKartQ_Table.csv'
track_file_name = '../analysis/Track_Table.csv'

# pos_array = np.array([[1, 1], [2, 3], [3, 8]])
# pos_array = np.array([[1, 1], [2, 2], [3, 3]])
# vel_vec = np.array([[0], [0]])

# tr = track.GoKartTrack()
# tr.proj_point_line_segment([199.67684115283683, 2.479025687543428], [200.32315884716317, -2.479025687543428], np.array([[153.62055745, 0.62971828]]))
# tr.create_track_spline()
# tr.save_track(track_file_name)

# spline = tr.load_track(track_file_name)  # Horizon Check
# tr.quadratic_tangent(np.array([[2, 1], [3, 0], [-1, 1]]))
# pt_a, pt_b = tr.equidistant_pts_tangent([0, 2], 2, 0)

# tr = track.GoKartTrack()   #  Track diff angle check
# tr.create_track_spline()
# tr_split = tr.split_spline(sim_param.track_sampling_distance)
# tr_bound_a, tr_bound_b, tr_angle = tr.track_boundary(angle=True)
# tr_angle.extend([tr_angle[-1]] * (sim_param.track_diff_horizon + sim_param.track_horizon_sight))
# horizon_array = []
# for iter in range(len(tr_bound_a)):
#     angle_hor = tr.diffrential_angle_curve_horizon(tr_angle, iter)
#     horizon_array.append(angle_hor)
# print((horizon_array))
# print(tr_angle)

# tr_checkpoint = tr.split_spline(50, spline)
# tr_check_a, tr_check_b = tr.track_boundary(tr_checkpoint)
# disp.plot_track(track_2=tr_bound_a, track_3=tr_bound_b)
# disp.plot_checkpoints(tr_check_a, tr_check_b)

# proj = tr.proj_point_line_segment([1, -7], [3, -1], np.array([[-4, 2]]))

#
sim_exe = sim(sim_param.total_runs)  # Simulation for learning
# print(sim_exe.round_learning_state_vector([1.6, 0.07, 0.07, 3.04]))
pos_mat = sim_exe.sim_run()
disp.plot_gokart_pos(pos_mat)
disp.plot_track(track_2=sim_exe.tr_bound_inner, track_3=sim_exe.tr_bound_outer)
disp.plot_checkpoints(sim_exe.tr_check_a, sim_exe.tr_check_b)
disp.show_plots()

#
# q_table = q.GoKartQTable()  # Create Q Table
# q_table.create_table()
# q_table.save_table(qTable_file_name)


# q_table = q.GoKartQTable()
# q_table.load_table(qTable_file_name)
# state, action, q_value, bool = q_table.get_q_value([28.0, 0.0, 0.22, 4.0])
# print('Before Mod : ', state, action, q_value)
# q_table.add_q_value(state, 0, 10.0)
# state, action, q_value, bool = q_table.get_q_value([28.0, 0.0, 0.22, 4.0])
# print('After Mod : ', state, action, q_value)
# q_table.update_q_value(state, action, 100.0)
# q_table.save_table(qTable_file_name)



# print(pos[0][0], pos[1][0], rad)
# print(accel_vec_update, accel_mag_update)
# print(pos_update)
# print(vel_vec_update, vel_mag_update)
