""" This file has the class declaration and the functions for handling the go-kart state update and the input from the
reinforcement learning interface for the whole simulation"""

import GoKartSim_Step as step
import GoKartTrack as track
from GoKartSimParam import GoKartParam as gp
from GoKartSimParam import SimParam as sim
import  GoKartQLearning as qlearn
import GoKartPlot as disp
import math
import numpy as np


class GoKartSim(object):

    def __init__(self, total_runs):

        self.run = True
        self.total_runs = total_runs
        self.step = step.GoKartSim_Step(sim.samp_time)
        self.start_pos = sim.start_pos
        self.start_vel = sim.start_vel
        self.start_accel = sim.start_accel
        self.throttle = 0.0
        self.steer = 0.0
        self.step_no = 0

        self.tr = track.GoKartTrack()
        self.spline = self.tr.load_track(sim.track_file_name)
        self.tr_split = self.tr.split_spline(sim.track_sampling_distance)
        self.tr_bound_inner, self.tr_bound_outer, self.tr_angle = self.tr.track_boundary(angle=True)
        self.tr_angle.extend([self.tr_angle[-1]] * (sim.track_diff_horizon + sim.track_horizon_sight))
        self.tr_checkpoint = self.tr.split_spline(sim.track_checkpoint_distance, self.spline)
        self.tr_check_a, self.tr_check_b = self.tr.track_boundary(self.tr_checkpoint)

        self.q_learn = qlearn.GoKartQLearning(learning=sim.learning_bool)

        self.sim_pos_array = []
        self.sim_samp_array = []
        self.sim_vel_array = []
        self.sim_accel_array = []
        self.sim_track_check_array = []  # Vector for saving the states of the go-kart per checkpoint
        self.sim_learning_state_array = []
        self.sim_learning_state_vector = []
        self.sim_learning_state_vector_round = []
        self.total_state_breach_runs = 0


    def sim_run(self):

        for run in range(self.total_runs):

            print('----------**** Run %d *****----------' % run)
            self.reintialize_variables()
            self.q_learn.reinitialise_run()
            check_time = 0.0
            box_index = 0
            checkpoint_index = 0
            prev_box_index = 0
            prev_checkpoint_index = 0
            vel_mag = self.sim_vel_array[0][2]
            pos_upd = np.array([self.sim_pos_array[-1]])
            theta_kart = 0.0
            reward_track = 0.0
            reward_check_collision = 0.0
            checkpoint_crossed = False
            reward_bool = False
            collision_bool = False
            state_breach_bool = False

            while self.run:
                # print('Step', self.step_no)
                checkpoint_crossed = False
                reward_bool = False
                reward_check_collision = 0.0

                if self.step_no >= sim.max_sim_steps:
                    break

                if self.step_no > 0:
                    reward_bool = True

                self.run, box_index, inner_distance, track_index = self.tr.check_on_track(pos_upd, self.tr_bound_inner,
                                                                                        self.tr_bound_outer, box_index)

                check_bool, checkpoint_index, check_inner_dist, check_track_index = \
                                    self.tr.check_on_track(pos_upd, self.tr_check_a, self.tr_check_b, checkpoint_index)

                if self.step_no % (sim.action_samp_time/sim.samp_time) == 0 or not self.run:  # Action sampling

                    reward_track = box_index - prev_box_index

                    if prev_checkpoint_index < checkpoint_index:  # If checkpoint is crossed

                        checkpoint_crossed = True
                        reward_check_collision = sim.track_checkpoint_distance / (check_time * sim.max_vel)
                        reward_check_collision = reward_check_collision + sim.reward_checkpoint
                        prev_checkpoint_index = checkpoint_index
                        check_time = 0.0

                    if not self.run and (box_index - prev_box_index) > sim.collision_threshold:

                        collision_bool = True
                        print('Collision : ', track_index)
                        reward_check_collision = sim.reward_kart_collision

                    angle_curve_horizon = self.tr.diffrential_angle_curve_horizon(self.tr_angle, track_index)
                    self.sim_learning_state_vector = [vel_mag, self.tr_angle[track_index] - theta_kart,
                                                      angle_curve_horizon, inner_distance]
                    self.sim_learning_state_vector_round = \
                                                       self.round_learning_state_vector(self.sim_learning_state_vector)

                    state_breach_bool = self.q_learning_function(self.sim_learning_state_vector_round, reward_track, reward_bool,
                                             reward_check_collision, checkpoint_crossed, collision_bool)
                    prev_box_index = box_index
                    self.sim_learning_state_array.append(self.sim_learning_state_vector_round)
                if not self.run or state_breach_bool:
                    break

                pos_centre_vec, rad = self.step.centri_update(np.array(self.sim_pos_array[-3:]))
                accel_vec, accel_mag, theta_kart, max_accel_bool = self.step.accel_update(pos_centre_vec, rad,
                                                        np.array(self.sim_pos_array[-2:]), self.sim_vel_array[-1][2],
                                                            self.sim_accel_array[-1][2], self.throttle, self.steer)
                vel_mat = np.transpose(np.array([self.sim_vel_array[-1][0:2]]))
                pos_upd = self.step.pos_update(self.sim_pos_array[-1], vel_mat, accel_vec)
                vel_upd, vel_mag = self.step.vel_update(vel_mat, accel_vec)

                self.sim_pos_array.append(pos_upd.tolist()[0])
                self.sim_vel_array.append([vel_upd[0, 0], vel_upd[1, 0], math.sqrt(vel_upd[0, 0] ** 2 +
                                                                                   vel_upd[1, 0] ** 2)])
                self.sim_accel_array.append([accel_vec[0][0], accel_vec[1][0], accel_mag])
                self.step_no += 1
                check_time += sim.samp_time
            if self.step_no >= sim.max_sim_steps:
                print('Simulation time exceeded')

            print('Collision States : ', self.sim_learning_state_array[-sim.actions_penalised_collision:])
            print(self.sim_learning_state_array)
            if state_breach_bool:
                self.total_state_breach_runs += 1
        print('State Breaches : ', self.total_state_breach_runs)
        if self.q_learn.learning:
            self.q_learn.table.save_table(sim.qTable_file_name)

        return self.sim_pos_array

    def q_learning_function(self, sim_learning_state_vector, prev_reward, reward_bool, reward_check_collision,
                            checkpoint_bool, collision_bool):

        breach_bool = False

        if not collision_bool:

            action, breach_bool = self.q_learn.select_action(sim_learning_state_vector)
            self.throttle, self.steer = self.action_to_input(action)

        if self.q_learn.learning:

            if reward_bool and not collision_bool and not breach_bool:
                self.q_learn.update_reward_sample(prev_reward, reward_bool)
            if checkpoint_bool:
                self.q_learn.update_reward_checkpoint_collision_breach(False, False, reward_check_collision)
            if collision_bool:
                self.q_learn.update_reward_checkpoint_collision_breach(True, False, reward_check_collision)
            if breach_bool:
                self.q_learn.update_reward_checkpoint_collision_breach(False, True)
            if breach_bool and collision_bool:
                self.q_learn.update_reward_checkpoint_collision_breach(True, True)

        self.q_learn.store_old_state()

        return breach_bool


    def reintialize_variables(self):

        self.sim_pos_array = [[self.start_pos[0][0] - 0.002, self.start_pos[0][1]],
                              [self.start_pos[0][0] - 0.001, self.start_pos[0][1]],
                              self.start_pos[0]]
        self.sim_samp_array = []  # Vector for saving the states of the go-kart per sampling time
        self.sim_vel_array = [self.start_vel[0]]
        self.sim_accel_array = [self.start_accel[0]]
        self.sim_track_check_array = []  # Vector for saving the states of the go-kart per checkpoint
        self.sim_learning_state_array = []
        self.run = True
        self.step_no = 0

    def round_learning_state_vector(self, vector):

        vector[0] = round(vector[0] / sim.vel_incr) * sim.vel_incr
        vector[1] = round(vector[1] / sim.theta_steer_incr) * sim.theta_steer_incr
        vector[2] = round(vector[2] / sim.theta_curve_incr) * sim.theta_curve_incr
        vector[3] = round(vector[3] / sim.dist_curve_incr) * sim.dist_curve_incr

        return vector

    def action_to_input(self, action):

        if action == 0:
            throttle = sim.inc_throttle * sim.action_samp_time
            steer = 0.0
        elif action == 1:
            throttle = sim.dec_throttle * sim.action_samp_time
            steer = 0.0
        elif action == 2:
            throttle = 0.0
            steer = sim.steer_clock * sim.action_samp_time
        elif action == 3:
            throttle = 0.0
            steer = sim.steer_anticlock * sim.action_samp_time
        else:
            throttle = 0.0
            steer = 0.0

        return throttle, steer
