""" The class has the functions related to creation, saving and loading of the Q-Tables of the Go Kart and
the other operations of accessing and modifying the Q values for the GoKart simulation """

from GoKartSimParam import SimParam as sim
from GoKartSimParam import GoKartParam as kart
import numpy as np
import pandas as pd
import random


class GoKartQTable(object):

    def __init__(self):

        self.q_table = pd.DataFrame()
        self.file_name = None

        self.no_vel_steps = int((sim.max_vel-sim.min_vel) / sim.vel_incr)
        self.no_theta_steer_steps = int((sim.max_theta_steer - sim.min_theta_steer) / sim.theta_steer_incr)
        self.no_theta_curve_steps = int((sim.max_theta_curve_diff - sim.min_theta_curve_diff) / sim.theta_curve_incr)
        self.no_dist_steps = int((sim.max_dist_curve - sim.min_dist_curve) / sim.dist_curve_incr)

        self.q_nparray = np.array([])
        self.action_values = np.zeros(((self.no_vel_steps + 1) * (self.no_theta_steer_steps + 1) *
                                       (self.no_theta_curve_steps + 1) * (self.no_dist_steps + 1), sim.no_actions))

    def create_table(self):

        list_vel = []
        list_steer = []
        list_curve = []
        list_dist = []
        list_temp = []

        # For the creation of the velocity array
        for vel in range(self.no_vel_steps + 1):

            for steps in range((self.no_theta_steer_steps + 1) * (self.no_theta_curve_steps + 1) *
                               (self.no_dist_steps + 1)):

                list_vel.append(round(round(sim.min_vel + vel * sim.vel_incr / sim.vel_incr) *
                                      sim.vel_incr, sim.round_decimal))
        print(list_vel)
        # For the creation of the steer array
        for steer in range(self.no_theta_steer_steps + 1):

            for steps in range((self.no_theta_curve_steps + 1) * (self.no_dist_steps + 1)):

                list_temp.append(round(round((sim.min_theta_steer + steer * sim.theta_steer_incr) / sim.theta_steer_incr)
                                 * sim.theta_steer_incr, sim.round_decimal))

        for vel in range(self.no_vel_steps + 1):

            list_steer.extend(list_temp)

        # For the creation of the curve array
        list_temp = []
        for curve in range(self.no_theta_curve_steps + 1):

            for dist in range((self.no_dist_steps + 1)):

                list_temp.append(round(round((sim.min_theta_curve_diff + curve * sim.theta_curve_incr) / sim.theta_curve_incr)
                                 * sim.theta_curve_incr, sim.round_decimal))

        for steps in range((self.no_vel_steps + 1) * (self.no_theta_steer_steps + 1)):

            list_curve.extend(list_temp)

        # For the creation of the distance array
        list_temp = []
        for dist in range((self.no_dist_steps + 1)):

            list_temp.append(round(round((sim.min_dist_curve + dist * sim.dist_curve_incr) / sim.dist_curve_incr)
                             * sim.dist_curve_incr, sim.round_decimal))

        for steps in range((self.no_vel_steps + 1) * (self.no_theta_steer_steps + 1) *
                           (self.no_theta_curve_steps + 1)):

            list_dist.extend(list_temp)

        self.q_nparray = np.array([list_vel, list_steer, list_curve, list_dist])   # Join the various lists
        self.q_nparray = np.transpose(self.q_nparray)     # Transpose the joined list before merging action values
        self.q_nparray = np.concatenate((self.q_nparray, self.action_values), axis=1)  # Merge action values

        self.q_table = pd.DataFrame(self.q_nparray, columns=sim.q_table_columns)
        print(self.q_table)

    def load_table(self, file_name):

        self.q_table = pd.read_csv(file_name, sep='\t', dtype=float)
        print(self.q_table)
        if self.q_table.empty:
            print('Table is empty intialising the table')
            self.file_name = file_name
            self.create_table()

        elif list(self.q_table.columns.values) != sim.q_table_columns:
            print('Wrong Q-Table file selected')

        else:
            print('The Q-Table was loaded successfully')

    def save_table(self, file_name=None):

        if file_name:
            self.file_name = file_name

        if self.file_name is not None:

            dec_format = '%.' + str(sim.round_decimal) + 'f'
            print('Saving Q-Table')
            self.q_table.to_csv(self.file_name, sep='\t', encoding='utf-8', index=False, float_format=dec_format)
            print('Q-Table exported')

        else:
            print('Please enter a valid file name and directory')

    def get_q_value(self, state_array):

        states_breached_bool = False
        state = None
        action = None
        q_value_array = []

        if len(state_array) < sim.no_states:
            print('The no.of states in the array does not match up')

        else:
            state_array = [round(x, sim.round_decimal) for x in state_array]
            q_value_frame = self.q_table.loc[(self.q_table[sim.q_table_columns[0]] == state_array[0]) &
                                             (self.q_table[sim.q_table_columns[1]] == state_array[1]) &
                                             (self.q_table[sim.q_table_columns[2]] == state_array[2]) &
                                             (self.q_table[sim.q_table_columns[3]] == state_array[3])]
            if not q_value_frame.empty:
                state = q_value_frame.index.tolist()[0]  # The index for the state in the Q-Table
                q_value_array = q_value_frame[q_value_frame.columns[-sim.no_actions:]].values[0]
                q_value_array_nan = self.check_possible_actions(state_array, q_value_array)
                action = np.nanargmax(q_value_array_nan)  # Action with the minimum reward for the given state
                action_list = np.where(q_value_array_nan == q_value_array_nan[action])[0]  # List of actions possible
                action = np.random.choice(action_list)  # Selects a random action of selected list of actions

            else:
                states_breached_bool = True
                print('States Breached : ', state_array)

        return state, action, q_value_array, states_breached_bool  # The minimum Q-Value corresponding to the state and action

    def update_q_value(self, state, action, q_value):

        self.q_table.loc[[state], sim.q_table_columns[sim.no_states + action]] = q_value

    def add_q_value(self, state, action, q_value):

        self.q_table.loc[[state], sim.q_table_columns[sim.no_states + action]] = self.q_table.loc[[state],
                                                             sim.q_table_columns[sim.no_states + action]] + q_value

    def check_possible_actions(self, action_values, q_value_array):

        nan_q_values = np.array(q_value_array)

        if action_values[0] >= kart.max_vel:
            nan_q_values[0] = None

        if action_values[1] <= kart.min_vel:
            nan_q_values[1] = None

        if action_values[2] >= (sim.max_theta_steer + sim.steering_lock_zone):
            nan_q_values[2] = None

        if action_values[3] <= (sim.min_theta_steer - sim.steering_lock_zone):
            nan_q_values[3] = None

        return nan_q_values
