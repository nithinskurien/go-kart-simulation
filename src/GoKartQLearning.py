from GoKartSimParam import SimParam as sim_param
import numpy as np
import GoKartQTable as qtable
import random


class GoKartQLearning(object):

    def __init__(self, learning):

        self.learning = learning  # set to true if the tables are to be populated with training data
        self.current_checkpoint = 0
        self.run = 0
        self.table = qtable.GoKartQTable()
        self.table.load_table(sim_param.qTable_file_name)

        self.learning_rate = sim_param.learning_rate  # Learning rate
        self.gamma = sim_param.gamma  # Discounting rate

        # Exploration parameters
        self.epsilon = sim_param.epsilon  # Exploration rate
        self.max_epsilon = sim_param.max_epsilon  # Exploration probability at start
        self.min_epsilon = sim_param.min_epsilon  # Minimum exploration probability
        self.decay_rate = sim_param.decay_rate  # Exponential decay rate for exploration prob

        self.reward_array = []
        self.states_array_checkpoint = []
        self.states_array = []
        self.states_array_breach = []
        self.states_array_collision = []
        self.reward = 0
        self.states = []
        self.q_values = []
        self.state_row = None
        self.action = None

        self.prev_states = []
        self.prev_q_values = []
        self.prev_state_row = None
        self.prev_action = None
        self.exploit = True

    def reinitialise_run(self):

        self.reward_array.append(self.reward)
        self.run += 1
        self.epsilon = self.min_epsilon + (self.max_epsilon - self.min_epsilon) * np.exp(-self.decay_rate * self.run)
        # Exploration parameters
        self.reward = 0
        self.states = []
        self.q_values = []
        self.state_row = None
        self.action = None

        self.prev_states = []
        self.prev_q_values = []
        self.prev_state_row = None
        self.prev_action = None
        self.exploit = True

        self.states_array_checkpoint = []
        self.states_array = []
        self.states_array_breach = []
        self.states_array_collision = []

    def select_action(self, states):

        self.states = states
        states_breached_bool = False
        exp_exp_tradeoff = random.uniform(0, 1)

        self.state_row, self.action, self.q_values, states_breached_bool = self.table.get_q_value(states)

        if len(self.states_array_collision) > sim_param.actions_penalised_collision:

            self.states_array_collision = self.states_array[-sim_param.actions_penalised_collision:]

        if len(self.states_array_breach) > sim_param.actions_penalised_breach:

            self.states_array_breach = self.states_array[-sim_param.actions_penalised_breach:]

        if not states_breached_bool:

            if self.learning:

                if exp_exp_tradeoff > self.epsilon:  # If greater than epsilon, exploitation

                    self.exploit = True

                else:  # else exploration

                    self.exploit = False
                    self.action = np.random.choice(range(sim_param.no_actions))

            else:

                self.exploit = True

            self.states_array_checkpoint.append([self.state_row, self.action, self.q_values[self.action]])
            self.states_array_breach.append([self.state_row, self.action, self.q_values[self.action]])
            self.states_array_collision.append([self.state_row, self.action, self.q_values[self.action]])
            self.states_array.append([self.state_row, self.action, self.q_values[self.action]])

        return self.action, states_breached_bool

    def update_reward_sample(self, reward, update_bool):

        if update_bool:

            new_qvalue = self.prev_q_values[self.prev_action] + self.learning_rate * (
                    reward + self.gamma * np.max(self.q_values) - self.prev_q_values[self.prev_action])
            self.table.update_q_value(self.prev_state_row, self.prev_action, new_qvalue)
            self.reward += reward

    def update_reward_checkpoint_collision_breach(self, collision_bool, breach_bool, reward_check_collision=None):

        if collision_bool:

            state_collision_array_len = len(self.states_array_collision)

            if state_collision_array_len > 0:

                reward_per_state = reward_check_collision / state_collision_array_len  # Reward if collision

                for index in range(state_collision_array_len):

                    self.table.add_q_value(self.states_array_collision[index][0], self.states_array_collision[index][1],
                                           reward_per_state)

            self.states_array_collision = []
            print(self.states_array)
        elif breach_bool:

            state_breach_array_len = len(self.states_array_breach)

            if state_breach_array_len > 0:

                reward_per_state = sim_param.reward_state_breached / state_breach_array_len  # Reward if collision

                for index in range(state_breach_array_len):

                    self.table.add_q_value(self.states_array_breach[index][0], self.states_array_breach[index][1],
                                           reward_per_state)

            self.states_array_breach = []

        elif collision_bool and breach_bool:

            reward = sim_param.reward_state_breached + sim_param.reward_kart_collision
            self.states_array_collision = []
            self.states_array_breach = []

        else:

            q_states_track = len(self.states_array_checkpoint)

            if q_states_track > 0:

                reward_per_state = reward_check_collision

                for index in range(q_states_track):

                    self.table.add_q_value(self.states_array_checkpoint[index][0],
                                           self.states_array_checkpoint[index][1], reward_per_state)

            self.states_array_checkpoint = []

    def store_old_state(self):

        self.prev_states = self.prev_states
        self.prev_q_values = self.q_values
        self.prev_state_row = self.state_row
        self.prev_action = self.action

