""" The class has the parameters of the Go Kart and simulation """


class GoKartParam:

        max_vel = 28.0  # m/s
        min_vel = 0.0  # m/S
        max_accel = 9.2  # m/s^2
        max_decel = -9.2  # m/s^2
        weight = 75.0  # kg
        frict_coeff = 0.7
        g = 9.8  # m/s^2
        throttle = 1.0  # m/s^2
        steer = 0.02  # rad/sec


class SimParam:

        # Learning
        learning_bool = True

        # File Names
        qTable_file_name = '../analysis/GoKartQ_Table.csv'
        track_file_name = '../analysis/Track_Table.csv'

        # Simulation Parameters
        total_runs = 10  # Total runs
        round_decimal = 2  # Places to round decimal to avoid precision errors while checking equal to
        samp_time = 0.02  # secs
        max_sim_steps = 50 / samp_time  # Steps for simulation to run before stopping
        q_table_actions = ['incThrottle', 'decThrottle', 'steerClock', 'SteerAntiClock', 'noAction']
        q_table_columns = ['velocity', 'thetaSteer', 'thetaCurve', 'distCurve']
                        # The learning states and possible actions that can be performed to the given state
        q_table_columns.extend(q_table_actions)
        no_actions = len(q_table_actions)  # Actions that can be performed
        no_states = len(q_table_columns) - no_actions

        # Track Parameters
        track_columns = ['x', 'y']
        track_sampling_distance = 0.5  # m
        collision_threshold = 4.0 * track_sampling_distance
        track_checkpoint_distance = 15.0  # m
        track_width = 5.0  # m
        track_diff_horizon_dist = 1.0  # m
        track_horizon_sight_dist = 20.0  # m  Distance to look ahead to start horizon
        track_horizon_sight = int(track_horizon_sight_dist / track_sampling_distance)
        track_diff_horizon = int(track_diff_horizon_dist / track_sampling_distance)  # The no.of track angle samples the simulation looks ahead, i.e curvature horizon

        # State Parameters
        start_pos = [[0.1, 0.0]]  # Go-Kart starting position
        start_vel = [[0.0, 0.0, 0.0]]
        start_accel = [[0.0, 0.0, 0.0]]
        min_vel = 0.0  # m/s
        max_vel = GoKartParam.max_vel  # m/s
        min_theta_steer = -0.7  # radians
        max_theta_steer = 0.7  # radians
        min_theta_curve_diff = -0.5  # radians
        max_theta_curve_diff = 0.5  # radians
        min_dist_curve = 0.0  # m
        max_dist_curve = track_width  # m

        vel_incr = 2.0  # m/s
        theta_steer_incr = 0.05  # radians
        theta_curve_incr = 0.01  # radians
        dist_curve_incr = 0.5  # m

        # Action Parameters
        action_samp_time = 0.2  # secs
        inc_throttle = 4.0  # m/s^2 per sec
        dec_throttle = -4.0  # m/s^2 per sec
        steer_clock = -1.5  # rad per sec
        steer_anticlock = 1.5  # rad per sec
        steering_lock_zone = 0.05  # rad

        # Learning Parameters
        learning_rate = 0.8  # Learning rate
        gamma = 0.95  # Discounting rate

        # Exploration parameters
        epsilon = 1.0  # Exploration rate
        max_epsilon = 1.0  # Exploration probability at start
        min_epsilon = 0.01  # Minimum exploration probability
        decay_rate = 0.005  # Exponential decay rate for exploration prob

        # Reward Parameters
        actions_penalised_breach = 1  # No.of actions penalised before the state breach
        actions_penalised_collision = 1  # No.of actions penalised before the collision
        reward_checkpoint = 0.0  # Reward gained for crossing a checkpoint
        reward_track_samp = 1.0  # Reward for crossing each track sampling distance
        reward_state_breached = -1.0  # Reward when the state of the go kart is breached
        reward_kart_collision = -1.0  # Reward when the go kart collides at the boundary of the track
